//AIO.h aka advanced input/output
#ifndef AIO_H
#define AIO_H

#include <iostream>
#include <string>

using namespace std;

/*class aio
{
	public:
		stringin(string input);
		stringin(string input, bool newline);
		stringout(string input);
		stringout(string input, bool newline);
	protected:
		string buffer;
};*/

void stringin()
{ 
	string input;
	std::cin >> input;
	string buffer = input + "\n";
}

void stringin(bool newline)
{
	string input;
	std::cin >> input;
	if(newline)
	{
		string buffer = input + "\n"; 
	} else {
		string buffer = input; 
	}
}

void stringout(string input)
{ 
	std::cout << input + "\n";
} 

//newline/no newline print
void stringout(string input, bool newline)
{
	if(newline)
	{
		std::cout << input + "\n";
	} else {
		std::cout << input;
	}
}

#endif 