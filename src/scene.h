//SCENE.h
#ifndef SCENE_H
#define SCENE_H

#include <iostream>
#include <string>

using namespace std;

class scene
{
	public:
		int render(int screenx, int screeny);
	protected:
		int xaxis[640], yaxis[480];
		int xmin, xmax, ymin, ymax;
};	

#endif