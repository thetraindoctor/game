#include <iostream>
#include <string>
#include "aio.h"

using namespace std;

int main()
{
	stringout("Hello World!", true);
	stringout("Hello World!", false);
	stringout("Hello World!", true);
	stringin(true);
	stringin(false);
	return 0;
}